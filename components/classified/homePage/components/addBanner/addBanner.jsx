import React, { Component } from "react";
import style from "./addBanner.module.css";
class AddBanner extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <>
        <div className={style.addBanner}>
          <img
            className="m-hide"
            src="https://cdn.syarah.com/syarah/bundles/bannerAdd.png"
            alt=""
          />
          <div>
            <h3>حراج السيارات</h3>
            <p>تصفح آلاف إعلانات السيارات المعروضة من المعارض و الأفراد</p>
          </div>
          <a className="whtBtn" href="https://syarah.com/حراج-السيارات">
            تصفح الآن
          </a>
        </div>
      </>
    );
  }
}
export default AddBanner;
