import React, { Component } from "react";

import style from "./whyBuySection.module.css";
class WhyBuySection extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <>
        <section className={style.whyBuySection}>
          <div className="container">
            <h1 className="big">
              ليش تشتري سيارتك من <br />
              سيارة اونلاين؟{" "}
            </h1>
            <ul className={style.whyBuyUl}>
              <li>
                <div className={style.wBTxtCont}>
                  <h3>5 أيام ضمان استرجاع</h3>
                  <p>
                    ما في داعي تخاف من شراء المستعمل بعد اليوم، احنا نعطيك 5
                    أيام أو 250 كم تجرب فيها السيارة عند استلامها واذا فيها أي
                    مشكلة غير مذكورة في الفحص تقدر ترجعها وتسترد مالك.{" "}
                  </p>
                </div>
                <img
                  src="https://cdn.syarah.com/syarah/bundles/a1.png"
                  alt=""
                />
              </li>
              <li>
                <div className={style.wBTxtCont}>
                  <h3>مفحوصة ومضمونة</h3>
                  <p>
                    نفحص 80 نقطة في سياراتنا المستعملة من ميكانيكا وبودي وأمان،
                    بالإضافة نرفق معها تقرير موجز لعرض تاريخ السيارة، يعني
                    سياراتنا مفحوصة ومضمونة مع ضمانة فنية لمدة سنة مجاناً تريح
                    بالك.{" "}
                  </p>
                </div>
                <img
                  src="https://cdn.syarah.com/syarah/bundles/a2.png"
                  alt=""
                />
              </li>
              <li>
                <div className={style.wBTxtCont}>
                  <h3>اشتريها من البيت وحنا نوصلها لين عندك</h3>
                  <p>
                    الحين تشتري سيارتك اونلاين وانت قاعد في البيت او في شغلك،
                    اختر السيارة اللي تعجبك واحجزها بـ 500 ريال فقط وحنا نقوم
                    بالباقي نسجلها ونأمنها ونوصلها لين عندك. خطواتنا سهلة وتقدر
                    تتعقب حالة طلبك من جوالك.{" "}
                  </p>
                </div>
                <img
                  src="https://cdn.syarah.com/syarah/bundles/a3.png"
                  alt=""
                />
              </li>
              <li>
                <div className={style.wBTxtCont}>
                  <h3>أفضل الأسعار وأكبر تشكيلة في المملكة</h3>
                  <p>
                    ولى زمن تلف وتسافر جميع معارض المملكة عشان تلاقي سيارتك
                    المنشودة. حنا قمنا بهذا الشغل وقارنا جميع الأسعار وأخذنا
                    أفضلها وأقلها، بالإضافة وفرنا لك أكثر من 2,000 سيارة تختار
                    منها، وإذا ما لاقيت اللي تبيه، فريقنا من أخصائي مبيعات
                    السيارات يدورون المملكة ويجيبنولك سيارتك المنشودة{" "}
                  </p>
                </div>
                <img
                  src="https://cdn.syarah.com/syarah/bundles/a4.png"
                  alt=""
                />
              </li>
            </ul>
          </div>
        </section>
      </>
    );
  }
}
export default WhyBuySection;
