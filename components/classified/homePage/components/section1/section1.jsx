import React, { Component } from "react";
import Link from "next/link";
import style from "./section1.module.css";

class Section1 extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <>
        <section className={style.section1}>
          <div className="container">
            <div className={style.section1Content}>
              <h1>
                <span>الحين تقدر تشتري</span>
                <br />
                <strong>سيارتك من بيتك</strong>
              </h1>
              <p>جديد أو مستعمل وحنا نوصلها لين عندك</p>
              <Link href="/post/search?is_online=1">
                <a className="redBtn">
                  ابحث في <span>2,410</span>
                  سيارة اونلاين{" "}
                </a>
              </Link>
              <img
                className="m-show"
                src="https://cdn.syarah.com/syarah/bundles/1.png"
                alt=""
              />
              <div className={style.paymentMethodsContainer}>
                <h5>
                  <img
                    src="https://cdn.syarah.com/syarah/bundles/greenArmor.svg"
                    alt=""
                  />
                  <span>طرق دفع إلكترونية آمنة</span>
                </h5>
                <ul>
                  <li>
                    <img
                      src="https://cdn.syarah.com/syarah/bundles/Mada.svg"
                      alt=""
                    />
                  </li>
                  <li>
                    <img
                      src="https://cdn.syarah.com/syarah/bundles/Sadad.svg"
                      alt=""
                    />
                  </li>
                  <li>
                    <img
                      src="https://cdn.syarah.com/syarah/bundles/Visadark.svg"
                      alt=""
                    />
                  </li>
                  <li>
                    <img
                      src="https://cdn.syarah.com/syarah/bundles/mc.svg"
                      alt=""
                    />
                  </li>
                  <li>
                    <img
                      src="https://cdn.syarah.com/syarah/bundles/paypalstacked.svg"
                      alt=""
                    />
                  </li>
                  <li>
                    <img
                      src="https://cdn.syarah.com/syarah/bundles/BankTransfer.svg"
                      alt=""
                    />
                  </li>
                </ul>
              </div>
            </div>
            <img
              className="m-hide"
              src="https://cdn.syarah.com/syarah/bundles/1.png"
              alt=""
            />
          </div>
        </section>
      </>
    );
  }
}
export default Section1;
