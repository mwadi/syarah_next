import React, { Component } from "react";
import style from "./HowToBuyContainer.module.css"

class HowToBuyContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeImageIndex: 0,
      myImagesArr: [
        "https://cdn.syarah.com/syarah/bundles/h1.png",
        "https://cdn.syarah.com/syarah/bundles/h2.png",
        "https://cdn.syarah.com/syarah/bundles/h3.png",
        "https://cdn.syarah.com/syarah/bundles/h4.png",
      ],
      myBtnsArr: [
        { number: 1, text: "إبحث عن السيارة اللي تبيها" },
        { number: 2, text: "إحجزها بـ 500 ريال مباشرة" },
        { number: 3, text: "نبدأ بتسجيل وتأمين سيارتك" },
        { number: 4, text: "توصيل سيارتك لين عندك" },
      ],
    };
  }

  setNewImage = (index) => {
    this.setState({
      activeImageIndex: index,
    });
  };
  render() {
    return (
      <>
        <section className={style.howToBuyContainer}>
          <div className="container">
            <h1 className="big">
              كيف تشتري سيارتك من <br />
              سيارة اونلاين؟{" "}
            </h1>
            <div className={style.toggleImagesContainer}>
              <ul>
                {this.state.myBtnsArr.map((item, i) => {
                  return (
                    <li
                      key={i}
                      onClick={() => {
                        this.setNewImage(i);
                      }}
                      className={
                        this.state.activeImageIndex === i
                          ? style.activeHow
                          : undefined
                      }
                    >
                      <strong>{item.number}</strong>
                      <span>{item.text}</span>
                      <img
                        className="m-show"
                        src={this.state.myImagesArr[i]}
                        alt=""
                      />
                    </li>
                  );
                })}
              </ul>
              <div>
                <img
                  src={this.state.myImagesArr[this.state.activeImageIndex]}
                  alt=""
                />
              </div>
            </div>
          </div>
        </section>
      </>
    );
  }
}

export default HowToBuyContainer;
