import React, { Component } from "react";
import style from "./CarShapeContainer.module.css";

class CarShapeContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <>
        <section className={style.carShapeContainer}>
          <div className="container">
            <h1 className="big">تصفح السيارت حسب الشكل</h1>
            <div className={style.chooseShapeContainer}>
              <a
                href="https://syarah.com/post/search?is_online=0&amp;car_shape=2"
                className={style.singleLogoMake}
              >
                <div className={style.shape_2}>
                  <span>سيدان</span>
                  <span>8,223</span>
                  <span>سيارة</span>
                </div>
              </a>
              <a
                href="https://syarah.com/post/search?is_online=0&amp;car_shape=5"
                className={style.singleLogoMake}
              >
                <div className={style.shape_5}>
                  <span>كروس اوفر</span>
                  <span>3,222</span>
                  <span>سيارة</span>
                </div>
              </a>
              <a
                href="https://syarah.com/post/search?is_online=0&amp;car_shape=4"
                className={style.singleLogoMake}
              >
                <div className={style.shape_4}>
                  <span>أس يو في</span>
                  <span>3,129</span>
                  <span>سيارة</span>
                </div>
              </a>
              <a
                href="https://syarah.com/post/search?is_online=0&amp;car_shape=3"
                className={style.singleLogoMake}
              >
                <div className={style.shape_3}>
                  <span>بيك أب</span>
                  <span>1,389</span>
                  <span>سيارة</span>
                </div>
              </a>
              <a
                href="https://syarah.com/post/search?is_online=0&amp;car_shape=6"
                className={style.singleLogoMake}
              >
                <div className={style.shape_6}>
                  <span>رياضية</span>
                  <span>468</span>
                  <span>سيارة</span>
                </div>
              </a>
              <a
                href="https://syarah.com/post/search?is_online=0&amp;car_shape=8"
                className={style.singleLogoMake}
              >
                <div className={style.shape_8}>
                  <span>فان</span>
                  <span>441</span>
                  <span>سيارة</span>
                </div>
              </a>
              <a
                href="https://syarah.com/post/search?is_online=0&amp;car_shape=7"
                className={style.singleLogoMake}
              >
                <div className={style.shape_7}>
                  <span>هاتشباك</span>
                  <span>296</span>
                  <span>سيارة</span>
                </div>
              </a>
              <a
                href="https://syarah.com/post/search?is_online=0"
                className={[
                  style.viewAllshapes,
                  "m-show",
                  style.mobileGoTo,
                ].join(" ")}
              >
                جميع السيارات
              </a>
            </div>
            <a
              href="https://syarah.com/post/search?is_online=0"
              className={[style.viewAllshapes, "m-hide", "whtBtn"].join(" ")}
            >
              جميع السيارات
            </a>
          </div>
        </section>
      </>
    );
  }
}

export default CarShapeContainer;
