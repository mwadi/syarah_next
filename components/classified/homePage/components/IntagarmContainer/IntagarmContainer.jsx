import React, { Component } from "react";
import style from "./IntagarmContainer.module.css";

class IntagarmContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      videosObj: [
        {
          videoSrc:
            "https://cdn.syarah.com/global/testimonials/1/1609066839-708.mp4",
          imageSrc:
            "https://cdn.syarah.com/global/testimonials/1/1609067355-589.png",
          userName: "حسين القحطاني",
          userLocation: "عسير",
        },
        {
          videoSrc:
            "https://cdn.syarah.com/global/testimonials/2/1609066973-1000.mp4",
          imageSrc:
            "https://cdn.syarah.com/global/testimonials/2/1609066972-523.png",
          userName: "ناصر القحطاني",
          userLocation: "الرياض",
        },
        {
          videoSrc:
            "https://cdn.syarah.com/global/testimonials/3/1609067273-531.mp4",
          imageSrc:
            "https://cdn.syarah.com/global/testimonials/3/1609067273-909.png",
          userName: "حسين القحطاني",
          userLocation: "عسير",
        },
        {
          videoSrc:
            "https://cdn.syarah.com/global/testimonials/4/1609067432-336.mp4",
          imageSrc:
            "https://cdn.syarah.com/global/testimonials/4/1609067432-652.png",
          userName: "ناصر القحطاني",
          userLocation: "الرياض",
        },
        {
          videoSrc:
            "https://cdn.syarah.com/global/testimonials/5/1609067592-989.mp4",
          imageSrc:
            "https://cdn.syarah.com/global/testimonials/5/1609067592-70.jpg",
          userName: "حسين القحطاني",
          userLocation: "عسير",
        },
      ],
      videoToWatch: undefined,
      activeSlideIndex: 0,
      showPopUp: false,
    };
  }

  setVideoToWatch = (url) => {
    this.setState({
      videoToWatch: url,
    });
  };

  shiftIndex = (dir) => {
    this.setState({
      activeSlideIndex:
        dir === "LL"
          ? this.state.activeSlideIndex === this.state.videosObj.length - 1
            ? 0
            : this.state.activeSlideIndex + 1
          : this.state.activeSlideIndex === 0
          ? this.state.videosObj.length - 1
          : this.state.activeSlideIndex - 1,
    });
  };

  //   openVideoPopUp = () => {
  //     showPopUp
  //   };

  render() {
    return (
      <>
        <section className={style.intagarmContainer}>
          <div className="container">
            <h1>
              <img
                src="https://cdn.syarah.com/syarah/bundles/instagram.svg"
                alt=""
              />
              <strong>#سيارتي ـ من ـ سيارة</strong>
              <span>شاهد تجارب العملاء مع سيارة اونلاين</span>
            </h1>
            <div className={style.instagramSlider}>
              <div className={style.slides}>
                {this.state.videosObj.map((item, i) => {
                  return (
                    <div
                      className={[
                        style.singleinstaSlide,
                        i === this.state.activeSlideIndex
                          ? style.activeIG
                          : undefined,
                        i === this.state.activeSlideIndex - 1
                          ? style.prevactiveIG
                          : undefined,
                        this.state.activeSlideIndex === 0 &&
                        i === this.state.videosObj.length - 1
                          ? style.lastOnFirst
                          : undefined,
                        this.state.activeSlideIndex ===
                          this.state.videosObj.length - 1 && i === 0
                          ? style.firstOnLast
                          : undefined,
                      ].join(" ")}
                      onClick={() => this.setVideoToWatch(item.videoSrc)}
                    >
                      <img src={item.imageSrc} alt="" />
                      <div className={style.layerIg}>
                        <div>
                          <strong>{item.userName}</strong>
                          <span>{item.userLocation}</span>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
              <div className={style.buttons}>
                <span
                  onClick={() => {
                    this.shiftIndex("RR");
                  }}
                  className={style.shiftRightIG}
                ></span>
                <span
                  onClick={() => {
                    this.shiftIndex("LL");
                  }}
                  className={style.shiftLeftIG}
                ></span>
              </div>
            </div>
          </div>
        </section>
      </>
    );
  }
}

export default IntagarmContainer;
