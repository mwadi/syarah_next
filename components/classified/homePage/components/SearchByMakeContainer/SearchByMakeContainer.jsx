import React, { Component } from "react";
import style from "./SearchByMakeContainer.module.css";

class SearchByMakeContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAllMakes: false,
    };
  }

  toggleAllMakes = () => {
    this.setState({
      showAllMakes: !this.state.showAllMakes,
    });
  };
  render() {
    return (
      <>
        <section className={style.searchByMakeContainer}>
          <div className="container">
            <h1 className="big">تصفح السيارت حسب الماركة</h1>
            <div className={style.makesWithLogo}>
              <a
                href="https://syarah.com/post/search?is_online=0&amp;make_id=4"
                className={style.singleLogoMake}
              >
                <div className={style.make_4}>
                  <span>4,058</span>
                  <span>سيارة</span>
                </div>
              </a>
              <a
                href="https://syarah.com/post/search?is_online=0&amp;make_id=60"
                className={style.singleLogoMake}
              >
                <div className={style.make_60}>
                  <span>2,965</span>
                  <span>سيارة</span>
                </div>
              </a>
              <a
                href="https://syarah.com/post/search?is_online=0&amp;make_id=38"
                className={style.singleLogoMake}
              >
                <div className={style.make_38}>
                  <span>1,204</span>
                  <span>سيارة</span>
                </div>
              </a>
              <a
                href="https://syarah.com/post/search?is_online=0&amp;make_id=58"
                className={style.singleLogoMake}
              >
                <div className={style.make_58}>
                  <span>1,072</span>
                  <span>سيارة</span>
                </div>
              </a>
              <a
                href="https://syarah.com/post/search?is_online=0&amp;make_id=37"
                className={style.singleLogoMake}
              >
                <div className={style.make_37}>
                  <span>940</span>
                  <span>سيارة</span>
                </div>
              </a>
              <a
                href="https://syarah.com/post/search?is_online=0&amp;make_id=35"
                className={style.singleLogoMake}
              >
                <div className={style.make_35}>
                  <span>880</span>
                  <span>سيارة</span>
                </div>
              </a>
              <a
                href="https://syarah.com/post/search?is_online=0&amp;make_id=48"
                className={style.singleLogoMake}
              >
                <div className={style.make_48}>
                  <span>727</span>
                  <span>سيارة</span>
                </div>
              </a>
              <a
                href="https://syarah.com/post/search?is_online=0&amp;make_id=53"
                className={style.singleLogoMake}
              >
                <div className={style.make_53}>
                  <span>575</span>
                  <span>سيارة</span>
                </div>
              </a>
              <a
                href="https://syarah.com/post/search?is_online=0&amp;make_id=69"
                className={style.singleLogoMake}
              >
                <div className={style.make_69}>
                  <span>493</span>
                  <span>سيارة</span>
                </div>
              </a>
            </div>

            {!this.state.showAllMakes && (
              <button
                onClick={() => {
                  this.toggleAllMakes();
                }}
                className={["whtBtn", style.viewAllMakes].join(" ")}
              >
                شاهد جميع الماركات
              </button>
            )}
            {this.state.showAllMakes && (
              <div className={style.allCarsMakeText}>
                <h3>جميع الماركات</h3>
                <ul>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=69">
                      شانجان (493)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=78">
                      ام جي (478)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=19">
                      جي إم سي (424)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=85">
                      هافال (344)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=5">
                      هوندا (325)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=9">
                      ايسوزو (316)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=51">
                      مازدا (299)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=55">
                      ميتسوبيشي (287)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=23">
                      دودج (165)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=26">
                      رينو (159)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=67">
                      جيلي (155)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=15">
                      بي ام دبليو (147)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=90">
                      جي أيه سي (145)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=36">
                      شيري (128)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=18">
                      جريت وول (117)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=47">
                      لاند روفر (102)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=33">
                      سوزوكي (89)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=66">
                      فاو (77)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=8">
                      اودي (70)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=44">
                      كرايسلر (63)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=16">
                      بيجو (58)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=43">
                      كاديلاك (56)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=95">
                      جيتور (55)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=20">
                      جيب (51)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=10">
                      انفنتي (43)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=98">
                      جينيسيس (39)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=13">
                      بورش (37)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=40">
                      فولكسفاغن (27)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=62">
                      اخرى (22)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=17">
                      جاغوار (21)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=50">
                      لينكولن (20)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=41">
                      فيات (18)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=12">
                      بنتلي (15)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=57">
                      ميني (14)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=21">
                      دايهاتسو (12)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=56">
                      ميركوري (11)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=68">
                      كلاسيكية (8)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=52">
                      مازيراتي (7)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=30">
                      ستيروين (7)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=25">
                      رولز رویس (7)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=39">
                      فولفو (6)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=32">
                      سوبارو (6)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=7">
                      الفا روميو (5)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=92">
                      ماكسيس (5)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=59">
                      همر (5)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=70">
                      ليفان (4)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=79">
                      فوتون (4)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=74">
                      بي واي دي (4)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=86">
                      سي إم سي (4)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=29">
                      سانج يونج (4)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=73">
                      فكتوريا اوتو (3)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=82">
                      جى إيه سي (3)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=97">
                      ماهيندرا (3)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=46">
                      لامبورغيني (2)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=96">
                      اشوك (2)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=14">
                      بونتياك (2)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=6">
                      استون مارتن (2)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=31">
                      سكودا (2)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=71">
                      جى إم سي (2)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=83">
                      هايجر (1)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=81">
                      جراند تايجر (1)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=77">
                      تاتا (1)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=91">
                      هينو (1)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=84">
                      بيوك (1)
                    </a>
                  </li>
                  <li>
                    <a href="https://syarah.com/post/search?is_online=0&amp;make_id=34">
                      سيات (1)
                    </a>
                  </li>
                </ul>
              </div>
            )}
          </div>
        </section>
      </>
    );
  }
}

export default SearchByMakeContainer;
