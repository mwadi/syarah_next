import React, { Component } from "react";
import styles from "./OurServices.module.css";

class OurServices extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <>
        <section className={styles.ourServices}>
          <div className="container">
            <h1 className="big">خدماتنا</h1>
            <div className={styles.servicesContent}>
              <div className={styles.singleService}>
                <div>
                  <div>
                    <img
                      src="https://cdn.syarah.com/syarah/bundles/s1.png"
                      alt=""
                    />
                  </div>
                </div>
                <h3>سيارة اونلاين</h3>
                <p>
                  خدمة مميزة من سيارة تساعدك على اختيار السيارة من بين الالاف
                  السيارات اللي تريد وتشتريها منا مباشرة ونقوم بكافة اجراءات
                  الفحص والتسجيل والتأمين ومن ثم توصيلها لين عندك وانت قاعد في
                  بيتك{" "}
                </p>
                <a
                  className="whtBtn"
                  href="https://syarah.com/post/search?is_online=1"
                >
                  اشتري سيارتك الحين
                </a>
              </div>
              <div className={styles.singleService}>
                <div>
                  <div>
                    <img
                      src="https://cdn.syarah.com/syarah/bundles/s2.png"
                      alt=""
                    />
                  </div>
                </div>
                <h3>سيارات المعارض</h3>
                <p>
                  إذا كنت ممن يفضلون التواصل مع المعارض وزيارتهم فلك خيار ذلك.
                  شراكتنا مع مئات المعارض حول المملكة وفرت لك آلالاف السيارات
                  لتغطي ميزانيتك او ذوقك.{" "}
                </p>
                <a className="whtBtn" href="https://syarah.com/سيارات-المعارض">
                  تصفح حراج السيارات
                </a>
              </div>
            </div>
          </div>
        </section>
      </>
    );
  }
}

export default OurServices;
