import React, { Component } from "react";
// import ReactDOM from "react-dom";
//import "../../syarahStyle.css";
import AddBanner from "./components/addBanner/addBanner";
import CarShapeContainer from "./components/CarShapeContainer/CarShapeContainer";
import HowToBuyContainer from "./components/HowToBuyContainer/HowToBuyContainer";
import IntagarmContainer from "./components/IntagarmContainer/IntagarmContainer";
import OurServices from "./components/OurServices/OurServices";
import SearchByMakeContainer from "./components/SearchByMakeContainer/SearchByMakeContainer";
import Section1 from "./components/section1/section1";
import WhyBuySection from "./components/whyBuySection/whyBuySection";
// import style from "./SyarahHomePage.module.css";
class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    // ReactDOM.render(<div>soooos</div>, document.getElementById("popUpId"));
  }

  render() {
    return (
      <>
        <Section1 />
        <AddBanner />
        <WhyBuySection />
        <OurServices />
        <HowToBuyContainer />
        <SearchByMakeContainer />
        <CarShapeContainer />
        <IntagarmContainer />
      </>
    );
  }
}
export default HomePage;
