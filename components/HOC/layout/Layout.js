import React, { Component } from "react";
import Link from "next/link";
import style from "./Layout.module.css";

class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.setBodyDir();
  }

  setBodyDir = () => {
    document.getElementsByTagName("body")[0].dir = "rtl";
    document.getElementsByTagName("html")[0].lang = "ar";
  };

  render() {
    return (
      <>
        <div
          className={
            this.props.authenticationMsg ? style.hasAuthPad : undefined
          }
        >
          <header
            className={[style.siteHeader, style.homePageMobileStyle].join("")}
          >
            <div className={["container", style.innerHdr].join(" ")}>
              <Link href="/">
                <img
                  src="https://cdn.syarah.com/syarah/bundles/logo.svg"
                  alt=""
                />
              </Link>
              <ul>
                <li>
                  <Link href="/">سيارات جديدة</Link>
                </li>
                <li>
                  <Link href="/">سيارات مستعملة</Link>
                </li>
                <li>
                  <Link href="/" className={style.onlineHdrLink}>
                    سيارة أونلاين
                  </Link>
                </li>
                <li>
                  <span className="sideMenuToggle notiMsgRed">
                    <img
                      src="https://cdn.syarah.com/syarah/bundles/menuToggle.svg"
                      alt=""
                    />
                  </span>
                </li>
              </ul>
            </div>
          </header>
          <main
            className={this.props.mainClass ? this.props.mainClass : undefined}
          >
            {this.props.children}
          </main>
          <div className="footer">
            <div className="container">
              <img
                src="https://cdn.syarah.com/syarah/bundles/ftrLogo.png"
                alt=""
              />
              <div className={style.dataFooterContent}>
                <div>
                  <ul>
                    <li>
                      <Link href="/">الرئيسية</Link>
                    </li>
                    <li>
                      <Link href="/site/about">من نحن</Link>
                    </li>
                    <li>
                      <Link href="/site/contact">اتصل بنا</Link>
                    </li>
                    <li>
                      <a
                        href="https://syarah.com/blog/"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        مدونة سيارة{" "}
                      </a>
                    </li>
                  </ul>
                  <p>شركة سعوديه بسجل تجاري 1010538980 مصدره الرياض</p>
                  <div>
                    <a
                      href="/site/terms"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      الأحكام والشروط{" "}
                    </a>{" "}
                    |
                    <a
                      href="/site/privacy-policy"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      سياسة الخصوصية{" "}
                    </a>
                  </div>
                </div>
                <div>
                  <strong className={style.greenGrd}>
                    <img
                      src="https://cdn.syarah.com/syarah/bundles/greenArmor.svg"
                      alt=""
                    />
                    <span>طرق دفع إلكترونية آمنة</span>
                  </strong>
                  <ul>
                    <li>
                      <img
                        src="https://cdn.syarah.com/syarah/bundles/Mada.svg"
                        alt=""
                      />
                    </li>
                    <li>
                      <img
                        src="https://cdn.syarah.com/syarah/bundles/Sadad.svg"
                        alt=""
                      />
                    </li>
                    <li>
                      <img
                        src="https://cdn.syarah.com/syarah/bundles/Visadark.svg"
                        alt=""
                      />
                    </li>
                    <li>
                      <img
                        src="https://cdn.syarah.com/syarah/bundles/mc.svg"
                        alt=""
                      />
                    </li>
                    <li>
                      <img
                        src="https://cdn.syarah.com/syarah/bundles/paypalstacked.svg"
                        alt=""
                      />
                    </li>
                    <li>
                      <img
                        src="https://cdn.syarah.com/syarah/bundles/BankTransfer.svg"
                        alt=""
                      />
                    </li>
                  </ul>
                  <p>جميع الحقوق محفوظة لشركة موقع سيارة المحدودة © 2020</p>
                </div>
              </div>
            </div>

            <div className={style.authenticationMsg}>
              <div className="container">
                <p>
                  <img
                    src="https://cdn.syarah.com/syarah/bundles/greenArmor.svg"
                    alt=""
                  />
                  هذا الموقع موثق من وزارة التجارة والاستثمار وبدعم من شركة علم,
                  بسجل تجاري رقم 1010538980{" "}
                </p>
                <img
                  src="https://cdn.syarah.com/syarah/bundles/saudi_elm.svg"
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default Layout;
