import React, { Component } from "react";
import Layout from "../components/HOC/layout/Layout";
import HomePage from "../components/classified/homePage/HomePage";

export default function Home() {
  return <HomePage />;
}

Home.Layout = Layout;
